<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['productCode', 'title', 'description', 'brand', 'partNumber', 'model', 'categoryId'];
    public $timestamps = true;
}
