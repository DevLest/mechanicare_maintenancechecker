<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    protected $fillable = ['total_items', 'total_price', 'discount', 'total_amount', 'cash', 'user_id', ];
    public $timestamps = true;
}
