<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;
use App\Inventory;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::select('products.*','category.title as categoryTitle', 'inventory.price', 'inventory.cost', 'inventory.price', 'inventory.quantity')->leftJoin('category', 'products.categoryId', '=', 'category.id')->leftJoin('inventory', 'products.id', '=', 'inventory.id')->get();
        $categories = Category::all();

        return view('products.index', ['products' => $products, 'categories' => $categories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->file('fileUploadProduct'))
        {
            $dataFile = $request->file('fileUploadProduct');
            $dataFile->move('uploads',$dataFile->getClientOriginalName());
            
            $newPath = public_path('uploads/'.$dataFile->getClientOriginalName());

            $productsArray = $this->csvToArray($newPath);

            for ($i = 0; $i < count($productsArray); $i ++)
            {
                $category = Category::where('id', $productsArray[$i]['category'])->first();

                if ( !is_null($category) ) 
                {
                    $isAvailable = Product::where( 'productCode', $productsArray[$i]['productcode'] )->first();
    
                    if ( is_null($isAvailable) )
                    {
                        $product = Product::create([
                            'productCode' => $productsArray[$i]['productcode'],
                            'title' => $productsArray[$i]['title'],
                            'description' => $productsArray[$i]['description'],
                            'brand' => $productsArray[$i]['brand'],
                            'partNumber' => intval( ($productsArray[$i]['partnumber'] != "") ? $productsArray[$i]['partnumber'] : null ),
                            'model' => $productsArray[$i]['model'],
                            'categoryId' => intval( ($productsArray[$i]['category'] != "") ? $productsArray[$i]['category'] : null ),
                        ]);
            
                        $inventory = Inventory::create([
                            'productId' => $product->id,
                            'quantity' => 0,
                            'cost' => 0,
                            'price' => 0,
                        ]);
                    }
                    else 
                    {
                        $isAvailable->productCode = $productsArray[$i]['productcode'];
                        $isAvailable->title = $productsArray[$i]['title'];
                        $isAvailable->description = $productsArray[$i]['description'];
                        $isAvailable->brand = $productsArray[$i]['brand'];
                        $isAvailable->partNumber = intval( ($productsArray[$i]['partnumber'] != "") ? $productsArray[$i]['partnumber'] : null );
                        $isAvailable->model = $productsArray[$i]['model'];
                        $isAvailable->categoryId = intval( ($productsArray[$i]['category'] != "") ? $productsArray[$i]['category'] : null );

                        $isAvailable->save();
                    }
                }
                else 
                {

                }
            }

            unlink($newPath);
            return response()->json(true);
        }
        else 
        {
            if ( $request->productId !== null ) if ($this->update($request) ) return redirect()->back();

            $product = Product::create([
                'productCode' => $request->productCode,
                'title' => $request->title,
                'description' => $request->description,
                'brand' => $request->brand,
                'partNumber' => $request->partNumber,
                'model' => $request->model,
                'categoryId' => $request->category,
            ]);

            $inventory = Inventory::create([
                'productId' => $product->id,
                'quantity' => 0,
                'cost' => 0,
                'price' => 0,
            ]);
            
            if ( $product && $inventory) return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if (\Illuminate\Support\Facades\Auth::user()->level != 1) return redirect('/pos');
        $product = Product::find($request->productId);

        $product->productCode = $request->productCode;
        $product->title = $request->title;
        $product->description = $request->description;
        $product->brand = $request->brand;
        $product->partNumber = $request->partNumber;
        $product->model = $request->model;
        $product->categoryId = $request->category;

        return $product->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function inventoryUpload( Request $request )
    {
        if (\Illuminate\Support\Facades\Auth::user()->level != 1) return redirect('/pos');
        if ($request->file('fileUpload')) 
        {
            $dataFile = $request->file('fileUpload');
            $dataFile->move('uploads',$dataFile->getClientOriginalName());
            
            $newPath = public_path('uploads/'.$dataFile->getClientOriginalName());

            $productsArray = $this->csvToArray($newPath);

            for ($i = 0; $i < count($productsArray); $i ++)
            {
                $productId = Product::where( 'productCode', $productsArray[$i]['productcode'] )->first();

                if ( !is_null($productId) ) {
                    $inventory = Inventory::where('id', $productId['id'])->first();
                
                    if (!is_null($inventory) ) {
                        $inventory['quantity'] = floatval($productsArray[$i]['quantity']);
        
                        $inventory->save();
                    }
                }
            }

            unlink($newPath);
        }
        return response()->json(true);
    }

    function csvToArray($filename = '', $delimiter = ',')
    {
        if (!file_exists($filename) || !is_readable($filename)) return false;

        $header = null;
        $data = array();
        if (($handle = fopen($filename, 'r')) !== false)
        {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false)
            {
                if (!$header)
                    $header = $row;
                else
                    $data[] = array_combine($header, $row);
            }
            fclose($handle);
        }

        return $data;
    }
}
