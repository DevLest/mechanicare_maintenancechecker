<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('category')->insert(['title' => 'Water Pump']);
        DB::table('category')->insert(['title' => 'Airpump']);
        DB::table('category')->insert(['title' => 'Battery Pump']);
        DB::table('category')->insert(['title' => 'AC / DC']);
        DB::table('category')->insert(['title' => 'Hikari']);
        DB::table('category')->insert(['title' => 'Fish Food']);
        DB::table('category')->insert(['title' => 'Fish Net']);
        DB::table('category')->insert(['title' => 'Magnet Cleaner']);
        DB::table('category')->insert(['title' => 'Aquarium']);
        DB::table('category')->insert(['title' => 'Fittings']);
        DB::table('category')->insert(['title' => 'Airstones']);
        DB::table('category')->insert(['title' => 'Dog Food']);
        DB::table('category')->insert(['title' => 'Cat Food']);
        DB::table('category')->insert(['title' => 'Bird Food']);
        DB::table('category')->insert(['title' => 'Filter Wools']);
        DB::table('category')->insert(['title' => 'Water Feeder']);
    }
}
