<div class="sidebar" data-color="color-theme" data-background-color="white" data-image="{{ asset('material') }}/img/sidebar-1.jpg">
  <!--
      Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

      Tip 2: you can also add an image using data-image tag
  -->
  <div class="logo">
    <a href="#" class="simple-text logo-normal">
      <i><img style="width:25px" src="{{ asset('material') }}/img/{{env('STORE_LOGO')}}"></i>
      &nbsp; {{env('STORE_TITLE')}}
    </a>
  </div>
  <div class="sidebar-wrapper">
    <ul class="nav">
      {{-- <li class="nav-item ">
        <a class="nav-link" href="./typography.html">
          <i class="material-icons">library_books</i>
          <p>Typography</p>
        </a>
      </li> --}}
      @if (Auth::user()->level == 1)
        <li class="nav-item{{ $activePage == 'dashboard' ? ' active' : '' }}">
          <a class="nav-link" href="{{ route('home') }}">
            <i class="material-icons">dashboard</i>
              <p>{{ __('Dashboard') }}</p>
          </a>
        </li>
        <li class="nav-item{{ $activePage == 'product-management' ? ' active' : '' }}">
          <a class="nav-link" href="{{ route('product.index') }}">
            <i class="material-icons">local_grocery_store</i>
              <p>{{ __('Product Management') }}</p>
          </a>
        </li>
        <li class="nav-item{{ $activePage == 'category' ? ' active' : '' }}">
          <a class="nav-link" href="{{ route('category.index') }}">
            <i class="material-icons">category</i>
              <p>{{ __('Category') }}</p>
          </a>
        </li>
        {{-- <li class="nav-item{{ $activePage == 'inventory-management' ? ' active' : '' }}">
          <a class="nav-link" href="{{ route('inventory.index') }}">
            <i class="material-icons">inventory_2</i>
              <p>{{ __('Inventory Management') }}</p>
          </a>
        </li> --}}
        <li class="nav-item{{ $activePage == 'profile' ? ' active' : '' }}">
          <a class="nav-link" href="{{ route('profile.edit') }}">
            <i class="material-icons">person</i>
              <p>{{ __('User profile') }}</p>
          </a>
        </li>
        <li class="nav-item{{ $activePage == 'user-management' ? ' active' : '' }}">
          <a class="nav-link" href="{{ route('user.index') }}">
            <i class="material-icons">person_add_alt_1</i>
              <p>{{ __('User List') }}</p>
          </a>
        </li>
      @endif
      
      @if (Auth::user()->level == 1 || Auth::user()->level == 3)
        <li class="nav-item{{ $activePage == 'transaction' ? ' active' : '' }}">
          <a class="nav-link" href="{{ route('transaction.index') }}">
            <i class="material-icons">shopping_cart</i>
              <p>{{ __('Transaction') }}</p>
          </a>
        </li>
        <li class="nav-item{{ $activePage == 'new-transaction' ? ' active' : '' }}">
          <a class="nav-link" href="{{ route('transaction.create') }}">
            <i class="material-icons">add_shopping_cart</i>
              <p>{{ __('New Transaction') }}</p>
          </a>
        </li>
      @endif 

      {{-- <li class="nav-item{{ $activePage == 'typography' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('typography') }}">
          <i class="material-icons">library_books</i>
            <p>{{ __('Typography') }}</p>
        </a>
      </li>
      <li class="nav-item{{ $activePage == 'icons' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('icons') }}">
          <i class="material-icons">bubble_chart</i>
          <p>{{ __('Icons') }}</p>
        </a>
      </li>
      <li class="nav-item{{ $activePage == 'map' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('map') }}">
          <i class="material-icons">location_ons</i>
            <p>{{ __('Maps') }}</p>
        </a>
      </li>
      <li class="nav-item{{ $activePage == 'notifications' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('notifications') }}">
          <i class="material-icons">notifications</i>
          <p>{{ __('Notifications') }}</p>
        </a>
      </li>
      <li class="nav-item{{ $activePage == 'language' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('language') }}">
          <i class="material-icons">language</i>
          <p>{{ __('RTL Support') }}</p>
        </a>
      </li>
      <li class="nav-item active-pro{{ $activePage == 'upgrade' ? ' active' : '' }} bg-danger">
        <a class="nav-link text-white" href="{{ route('upgrade') }}">
          <i class="material-icons">unarchive</i>
          <p>{{ __('Upgrade to PRO') }}</p>
        </a>
      </li> --}}
    </ul>
  </div>
</div>