<div class="modal fade bd-example-modal-lg" id="categoryAddModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Add New Category</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form method="post" action="{{ route('category.store') }}" class="form-horizontal">
                @csrf
                @method('post')
    
                <div class="card ">
                    <div class="card-header card-header-danger color-theme">
                        <h4 class="card-title">Category Details</h4>
                    </div>
                    <div class="card-body ">
                        @if (session('status_password'))
                        <div class="row">
                            <div class="col-sm-12">
                            <div class="alert alert-success">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <i class="material-icons">close</i>
                                </button>
                                <span>{{ session('status_password') }}</span>
                            </div>
                            </div>
                        </div>
                        @endif
                        <div class="row">
                        <label class="col-sm-5 col-form-label" for="productCode">Category Title:</label>
                        <div class="col-sm-7">
                            <div class="form-group">
                                <input class="form-control" name="title" id="modal-categorytitle" type="text" placeholder="Food, Tools, Vegetables . . . " value="" required />
                                <input name="categoryId" id="modal-category-Id" type="text" value="" hidden />
                            </div>
                        </div>
                        </div>
                    </div>
                    <div class="card-footer ml-auto mr-auto">
                        <button type="submit" class="btn btn-danger">Save Category</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
</div>