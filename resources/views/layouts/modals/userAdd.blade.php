<div class="modal fade bd-example-modal-lg" id="userAddModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Add New Category</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form method="post" action="{{ route('user.store') }}" class="form-horizontal">
                @csrf
                @method('post')
    
                <div class="card ">
                    <div class="card-header card-header-danger color-theme">
                        <h4 class="card-title">Category Details</h4>
                    </div>
                    <div class="card-body ">
                        @if (session('status_password'))
                        <div class="row">
                            <div class="col-sm-12">
                            <div class="alert alert-success">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <i class="material-icons">close</i>
                                </button>
                                <span>{{ session('status_password') }}</span>
                            </div>
                            </div>
                        </div>
                        @endif
                        <div class="row">
                            <label class="col-sm-5 col-form-label" for="modal-fullname">Fullname:</label>
                            <div class="col-sm-7">
                                <div class="form-group">
                                    <input class="form-control" name="fullname" id="modal-fullname" type="text" placeholder="Lastname, Firstname" value="" required />
                                    <input name="userId" id="modal-userId" type="text" value="" hidden />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-5 col-form-label" for="modal-username">Username:</label>
                            <div class="col-sm-7">
                                <div class="form-group">
                                    <input class="form-control" name="username" id="modal-username" type="text" placeholder="John Doe" value="" required />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-5 col-form-label" for="password">Password:</label>
                            <div class="col-sm-7">
                                <div class="form-group">
                                    <input class="form-control" name="password" id="modal-password" type="password" placeholder="********" value="" required />
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <label class="col-sm-5 col-form-label" for="modal-role">Role</label>
                            <div class="col-sm-7">
                                <div class="form-group">
                                    <select class="form-control" name="role" id="modal-role">
                                        @if (isset($roles))
                                            <option></option>

                                            @foreach ( $roles as $role )
                                                <option value={{$role->id}}>{{ $role->description }}</option>
                                            @endforeach
                                        @else
                                            <option>No Available Categories</option>
                                        @endif
                                    </select>
                                </div>
                        </div>
                    </div>
                    <div class="card-footer ml-auto mr-auto">
                        <button type="submit" class="btn btn-danger">Save User</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
</div>