@extends('layouts.app', ['activePage' => 'product-management', 'titlePage' =>'Product Managenent'])

@section('content')
  <div class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header card-header-danger color-theme">
              <h4 class="card-title ">Products</h4>
              <p class="card-category"> Here you can manage all product details and inventory</p>
            </div>
            
            <div class="card-body">
              <div class="row">
                <div class="col-12 text-right">
                  <a href="#" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#productAddModal" onclick="clearFields()">Add product</a>
                  <a href="#" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#uploadInventory">Upload Inventory</a>
                  {{-- <div class="file btn btn-success">
                    Upload Inventory
                    <input type="file" name="file" style="position: absolute;font-size: 50px;opacity: 0;right: 0;top: 0;" accept=".xls,.xlsx,.csv"/>
                  </div> --}}
                </div>
              </div>
              <div class="table-responsive">
                <table class="table" id="dataTable">
                  <thead class=" text-primary">
                    <tr>
                      <th>Product Code</th>
                      <th>Title</th>
                      <th>Description</th>
                      <th>Brand</th>
                      <th>Category</th>
                      <th>Cost</th>
                      <th>Price</th>
                      <th>Inventory Available</th>
                      @if ( auth()->user() )
                        <th class="text-right">
                          Actions
                        </th>
                      @endif
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ( $products as $product )
                        <tr>
                          <td>{{ $product->productCode }}</td>
                          <td>
                            {{ $product->title }}
                            <input type="text" hidden id="title{{ $product->productCode }}" value="{{ $product->title }}">
                          </td>
                          <td>
                            {{ $product->description }}
                            <input type="text" hidden id="description{{ $product->productCode }}" value="{{ $product->description }}">
                          </td>
                          <td>
                            {{ $product->brand }}
                            <input type="text" hidden id="brand{{ $product->productCode }}" value="{{ $product->brand }}">
                          </td>
                          <td>
                            {{ $product->categoryTitle }}
                            <input type="text" hidden id="category{{ $product->productCode }}" value="{{ $product->categoryId }}">
                          </td>
                          <td>{{ ($product->cost > 0) ? $product->cost : 0.00 }}</td>
                          <td>{{ ($product->price > 0) ? $product->price : 0.00 }}</td>
                          <td>{{ ($product->quantity > 0) ? $product->quantity : 0.00 }}</td>
                          @if ( auth()->user() )
                            <td class="td-actions text-right">
                              <button type="button" rel="tooltip" title="" class="btn btn-primary btn-link btn-sm" data-original-title="Edit" data-toggle="modal" data-target="#productAddModal" onclick="loadModal('{{$product->productCode}}', '{{$product->id}}')">
                                <i class="material-icons">edit</i>
                                <div class="ripple-container"></div>
                              </button>
                            </td>
                          @endif
                        </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
    </div>
  </div>
  <script>

    $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });

    function loadModal( productCode, productId )
    {
      $('#modal-productCode').val( productCode );
      $('#modal-title').val( $('#title'+productCode).val() );
      $('#modal-description').val( $('#description'+productCode).val() );
      $('#modal-brand').val( $('#brand'+productCode).val() );
      $('#modal-category').val( $('#category'+productCode).val() );
      $('#modal-productId').val( productId );
    }

    function clearFields()
    {
      $('#modal-productCode').val("");
      $('#modal-title').val("");
      $('#modal-description').val("");
      $('#modal-brand').val("");
      $('#modal-productId').val("");
    }

    function inventoryUpload()
    {
      $('#loader').removeAttr('hidden');
      $("#uploadButton").attr("hidden",true);

      let formData = new FormData($('#upload-file-form')[0]);

      $.ajax({
        type:'POST',
        url: `{{ route("product.inventory-upload") }}`,
        data: formData,
        contentType: false,
        processData: false,
        success: (response) => 
        {
          $("#loader").attr("hidden",true);
          $('#uploadButton').removeAttr('hidden');

          if (response) $('#uploadInventory').modal('hide');
          location.reload();
        },
        error: function(response)
        {
          console.log(response);
        }
      });
    }

    function productUpload()
    {
      $('#loader').removeAttr('hidden');

      let formData = new FormData($('#upload-product-form')[0]);

      $.ajax({
        type:'POST',
        url: `{{ route("product.store") }}`,
        data: formData,
        contentType: false,
        processData: false,
        success: (response) => 
        {
          $("#loader").attr("hidden",true);

          if (response) $('#productAddModal').modal('hide');
          location.reload();
        },
        error: function(response)
        {
          console.log(response);
        }
      });
    }
  </script>
@endsection