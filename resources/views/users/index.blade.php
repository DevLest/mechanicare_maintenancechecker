@extends('layouts.app', ['activePage' => 'user-management', 'titlePage' => __('User Managenent')])

@section('content')
  <div class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header card-header-danger color-theme">
              <h4 class="card-title ">Users</h4>
              <p class="card-category"> Here you can manage users</p>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-12 text-right">
                  <a href="#" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#userAddModal" onclick="clearFields()">Add user</a>
                </div>
              </div>
              <div class="table-responsive">
                <table class="table">
                  <thead class=" text-primary">
                    <tr><th>
                      Name
                    </th>
                    <th>
                      Username
                    </th>
                    <th>
                      Role
                    </th>
                    <th>
                      Creation date
                    </th>
                    @if ( auth()->user() )
                      <th class="text-right">
                        Actions
                      </th>
                    @endif
                  </tr></thead>
                  <tbody>
                    @foreach ( $users as $user )
                        <tr>
                          <td>{{ $user->name }}</td>
                          <input type="text" hidden id="name{{ $user->id }}" value="{{ $user->name }}">
                          <td>{{ $user->email }}</td>
                          <input type="text" hidden id="username{{ $user->id }}" value="{{ $user->email }}">
                          <td>{{ $user->role }}</td>
                          <input type="text" hidden id="role{{ $user->id }}" value="{{ $user->level }}">
                          <td>{{ $user->created_at }}</td>
                          @if ( auth()->user() )
                            <td class="td-actions text-right">
                              <button type="button" rel="tooltip" title="" class="btn btn-primary btn-link btn-sm" data-original-title="Edit" data-toggle="modal" data-target="#userAddModal" onclick="loadModal('{{$user->id}}')">
                                <i class="material-icons">edit</i>
                                <div class="ripple-container"></div>
                              </button>
                            </td>
                          @endif
                        </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
    </div>
  </div>
  <script>
    function loadModal( userId )
    {
      $('#modal-userId').val( userId );
      $('#modal-fullname').val( $('#name'+userId).val() );
      $('#modal-username').val( $('#username'+userId).val() );
      $('#modal-role').val( $('#role'+userId).val() );
      console.log($('#modal-userId').val());
    }

    function clearFields()
    {
      $('#modal-fullname').val("");
      $('#modal-userId').val("");
      $('#modal-username').val("");
      $('#modal-password').val("");
      $('#modal-role').val("");
    }
  </script>
@endsection